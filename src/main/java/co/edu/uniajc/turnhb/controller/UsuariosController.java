package co.edu.uniajc.turnhb.controller;

import co.edu.uniajc.turnhb.model.usuarioModel;
import co.edu.uniajc.turnhb.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import java.util.*;
import io.swagger.annotations.*;

@RestController
@RequestMapping("/users")
@Api("Users")
public class UsuariosController {
    private UsuarioService usuarioService;

    @Autowired
    public UsuariosController(UsuarioService usuarioService){ this.usuarioService = usuarioService; }
    
    @PostMapping(path = "/save")
    @ApiOperation(value="Insert Client",response = usuarioModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Algo ha fallado"),
            @ApiResponse(code = 500, message = "Error interno del servidor")
    })
    public usuarioModel saveUser(@RequestBody usuarioModel usuarioModel){
        return usuarioService.save(usuarioModel);
    }

    public usuarioModel getById(int id) {
        return usuarioService.getById(id);
    }
    @GetMapping(path = "/all")
    @ApiOperation(value = "Buscar todos los usuarios", response = usuarioModel.class)
    public List<usuarioModel> findAll() {
        return usuarioService.findAll();
    }

    public Optional<usuarioModel> findById(Long aLong) {
        return usuarioService.findById(aLong);
    }
    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "Borrar Usuario por ID", response = usuarioModel.class)
    public void deleteById(Long aLong) {
        usuarioService.deleteById(aLong);
    }
    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Usuario", response = usuarioModel.class)
    public usuarioModel update(usuarioModel usuarioModel) {
        return usuarioService.update(usuarioModel);
    }
}


