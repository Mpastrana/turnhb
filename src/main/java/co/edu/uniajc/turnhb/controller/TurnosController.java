package co.edu.uniajc.turnhb.controller;

import co.edu.uniajc.turnhb.model.turnosModel;
import co.edu.uniajc.turnhb.service.TurnoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import java.util.*;
import io.swagger.annotations.*;

@RestController
@RequestMapping("/turn")
@Api("Turns")
public class TurnosController {
    private TurnoService turnoService;

    @Autowired
    public TurnosController(TurnoService turnoService){ this.turnoService = turnoService; }
    
    @PostMapping(path = "/save")
    @ApiOperation(value="Insert Turn",response = turnosModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Algo ha fallado"),
            @ApiResponse(code = 500, message = "Error interno del servidor")
    })
    public turnosModel saveTurn(@RequestBody turnosModel turnosModel){
        return turnoService.save(turnosModel);
    }

    public turnosModel getById(int id) {
        return turnoService.getById(id);
    }
    @GetMapping(path = "/all")
    @ApiOperation(value = "Buscar todos los turnos", response = turnosModel.class)
    public List<turnosModel> findAll() {
        return turnoService.findAll();
    }

    public Optional<turnosModel> findById(Long aLong) {
        return turnoService.findById(aLong);
    }
    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "Borrar Turno por ID", response = turnosModel.class)
    public void deleteById(Long aLong) {
        turnoService.deleteById(aLong);
    }
    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Turno", response = turnosModel.class)
    public turnosModel update(turnosModel turnosModel) {
        return turnoService.update(turnosModel);
    }
}
