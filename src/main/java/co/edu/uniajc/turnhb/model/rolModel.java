package co.edu.uniajc.turnhb.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Rol")
public class rolModel implements Serializable{
	
	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    
    @Column(name="rol")
    private String rol;
        
    public rolModel() {
    	//Constructor
    }

    public rolModel(int id, String rol) {
        this.id = id;
        this.rol = rol;        
    }

    public long getId() { return id; }
    public void setId(int id) { this.id = id; }
   
    public String getRol() { return rol; }
    public void setRol(String rol) { this.rol = rol; }

}

