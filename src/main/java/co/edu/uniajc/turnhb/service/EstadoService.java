package co.edu.uniajc.turnhb.service;

import co.edu.uniajc.turnhb.model.estadosModel;
import co.edu.uniajc.turnhb.repository.EstadosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class EstadoService {
    private final EstadosRepository estadosRepository;
    @Autowired
    public EstadoService(EstadosRepository estadosRepository) {
        this.estadosRepository = estadosRepository;
    }

    public estadosModel getById(int id) {
        return estadosRepository.getById(id);
    }

    @Query(nativeQuery = true, value = "SELECT" + "id" + ", estado" + 
    		"FROM Estado" + " WHERE estado =: estado")
    
    public List<estadosModel> findEstado(int estado) {
        return estadosRepository.findEstado(estado);
    }

    public List<estadosModel> findAll() {
        return estadosRepository.findAll();
    }

    public List<estadosModel> findAll(Sort sort) {
        return estadosRepository.findAll(sort);
    }

    public <S extends estadosModel> S save(S entity) {
        return estadosRepository.save(entity);
    }

    public Optional<estadosModel> findById(Long aLong) {
        return estadosRepository.findById(aLong);
    }

    public boolean existsById(Long aLong) {
        return estadosRepository.existsById(aLong);
    }

    public long count() {
        return estadosRepository.count();
    }

    public void deleteById(Long aLong) {
        estadosRepository.deleteById(aLong);
    }
    public estadosModel update(estadosModel estadosModel){
        if(existsById(estadosModel.getId()) == true)
        {
            estadosRepository.saveAndFlush(estadosModel);
        }
        return estadosModel;
    }
}

