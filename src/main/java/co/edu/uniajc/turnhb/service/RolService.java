package co.edu.uniajc.turnhb.service;

import co.edu.uniajc.turnhb.model.rolModel;
import co.edu.uniajc.turnhb.repository.RolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class RolService {
    private final RolRepository rolRepository;
    @Autowired
    public RolService(RolRepository rolRepository) {
        this.rolRepository = rolRepository;
    }

    public rolModel getById(int id) {
        return rolRepository.getById(id);
    }

    @Query(nativeQuery = true, value = "SELECT" + "id" + ", rol" + 
    		"FROM Rol" + " WHERE id =: id")
    
    public List<rolModel> findRole(int id) {
        return rolRepository.findRole(id);
    }

    public List<rolModel> findAll() {
        return rolRepository.findAll();
    }

    public List<rolModel> findAll(Sort sort) {
        return rolRepository.findAll(sort);
    }

    public <S extends rolModel> S save(S entity) {
        return rolRepository.save(entity);
    }

    public Optional<rolModel> findById(Long aLong) {
        return rolRepository.findById(aLong);
    }

    public boolean existsById(Long aLong) {
        return rolRepository.existsById(aLong);
    }

    public long count() {
        return rolRepository.count();
    }

    public void deleteById(Long aLong) {
        rolRepository.deleteById(aLong);
    }
    public rolModel update(rolModel rolModel){
        if(existsById(rolModel.getId()) == true)
        {
            rolRepository.saveAndFlush(rolModel);
        }
        return rolModel;
    }
}
