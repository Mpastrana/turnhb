package co.edu.uniajc.turnhb.repository;

import co.edu.uniajc.turnhb.model.turnosModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TurnosRepository extends JpaRepository<turnosModel, Long>{
    List<turnosModel> findAllByNamesContains(String name);
    turnosModel getById(int idTurno);
    @Query(nativeQuery = true, value="SELECT"+
    "id_Turno"+",celularCliente"+",nombreCliente"+",apellidoCliente"+
    "FROM Turno"+" WHERE id_Turno =: id_Turno")
    List<turnosModel> findEstado(@Param(value = "id_Turno") Integer idTurno);
}



