package co.edu.uniajc.turnhb.repository;

import co.edu.uniajc.turnhb.model.usuarioModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<usuarioModel, Long>{
    List<usuarioModel> findAllByNamesContains(String name);
    usuarioModel getById(int id);
    @Query(nativeQuery = true, value="SELECT"+
    "id"+",celular"+",nombre"+",apellido"+",idrol"+
    "FROM Usuario"+" WHERE id =: id")
    List<usuarioModel> findEstado(@Param(value = "id") Integer id);
}
